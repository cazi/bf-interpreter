#include <stdio.h>

const int TAPE_SIZE = 30000;

int main(int argc, char** argv)
{
	unsigned char tape[TAPE_SIZE], ch;
	unsigned char *ptr = tape;
	FILE* fp;
	int i, j;

	if (argc == 1)
	{
		puts("Please enter a file(s)\n");
		return 0;
	}

	// if there is arguments
	for (i = 1; i < argc; i++)
	{
		// reset tape for each file
		for (j = 0; j < TAPE_SIZE; j++)
			tape[i] = 0;

		// open each file
		printf("File %s:\n", argv[i]);
		fp = fopen(argv[i], "r");
		if (fp == NULL)
		{
			// if the file doesn't exist
			printf("Cannot read/access file %s\n", argv[i]);
			continue;
		}
		
		while ((ch = getc(fp)) != EOF)
		{
			switch(ch)
			{
				case '>': ptr++;            break;
				case '<': ptr--;            break;
				case '+': *ptr += 1;        break;
				case '-': *ptr -= 1;        break;
				case '.': putchar(*ptr);    break;
				case ',': *ptr = getchar(); break;
				// case '[': ;
				// case ']': ;
			};
		}
		printf("\n");
	}

	return 0;
}
